var express = require('express');
var router = express.Router();
const warLogController = require('../controllers/warLogController.js')
const clanController = require('../controllers/clanController.js')

router.get('/', clanController.getClanInfo);
router.get('/war', warLogController.getClanStatistics);

module.exports = router;
