var express = require('express');
var router = express.Router();
const playerController = require('../controllers/playerController.js')

router.get('/:name', playerController.getPlayerInfo);

module.exports = router;
