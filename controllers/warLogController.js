var date_formatter = require('../helpers/date_formatter.js');
const reducer = require('../helpers/reducer.js');
const assert = require('assert');
var db = require('../db.js');
var WarLogStats = require('../models/war_log_stats.js');
var Member = require('../models/member.js');
var Statistic = require('../models/statistic.js');
var WarLog = require('../models/war_log.js');
var date_formatter = require('../helpers/date_formatter.js');
const beautify = require('js-beautify').html
const pug = require('pug');

var clanStatistics = function(clanMembers) {
  return clanMembers.map(clanMember => {
    return new Promise(function(resolve, reject) {
      getStatistics(clanMember.name).then(warData => {
        resolve(new WarLogStats(clanMember.name, warData));
      });
    });
  });
};

function getClanMembers() {
  return Member.find({}).sort({
    tag: -1
  });
}

function getStatistics(name) {
  return Statistic.find({
    name: name
  });
}

function getWarLogsTimeSeries(logs) {
  WarLog.find({}).sort({
    createdDate: 1
  }).then((warLogs) => {
    var x = warLogs.map(function(warlog) {
      var standings = warlog.standings;
      var clan = standings.find(function(item, i) {
        if (item.tag === '2VVYQC9')
          return (item)
      });
      return ({
        labels: date_formatter(warlog.createdDate * 1000),
        participants: clan.participants,
        battlesPlayed: clan.battlesPlayed,
        warTrophies: clan.warTrophies,
        wins: clan.wins
      })
    });
    logs(reducer(x));
  })
}

exports.getClanStatistics = (req, res, next) => {
  getClanMembers().then((clanMembers) => {
    getWarLogsTimeSeries((logs) => {
      Promise.all(clanStatistics(clanMembers)).then((statistics) => {
        statistics.sort((a, b) => (a.winRatio - b.winRatio) * -1);
        const template = pug.compileFile(__dirname + '/../views/index.pug');
        var html = template({
          title: 'Clan',
          message: 'Clan wars',
          statistics: statistics,
          warlogs: logs
        })
        res.send(beautify(html));
      })
    })
  })
}
