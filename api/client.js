const assert = require('assert');
var request = require('request');
var config = require("config");
const logger = require('../helpers/logger.js');

const options = {
  'auth': {
    'bearer': config.crapi.token
  }
};

exports.get = function(url) {
  // TODO: Se podría cambiar el cliente a axios
  return new Promise(function(resolve, reject) {
    request.get(url, options, function(error, response, body) {
      if (response.statusCode == 200) {
        resolve(JSON.parse(body));
      }else{
        logger.info(`error: ${error}`);
        logger.info(`response: ${response}`);
        logger.info(`body: ${body}`);
      }
    });
  })
}
