var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var db = require('../db');

var deckCardSchema = mongoose.Schema({
  name: String,
  level: Number,
  maxLevel: Number,
  rarity: String,
  requiredForUpgrade: Schema.Types.Mixed,
  icon: String,
  key: String,
  elixir: Number,
  type: String,
  arena: Number,
  description: String,
  id: Number
}, {
  _id: false
});

var teamSchema = mongoose.Schema({
  tag: String,
  name: String,
  crownsEarned: Number,
  trophyChange: Number,
  startTrophies: Number,
  clan: {
    tag: String,
    name: String,
    badge: {
      name: String,
      category: String,
      id: Number,
      image: String
    }
  },
  deckLink: String,
  deck: [deckCardSchema]
}, {
  _id: false
});

var battleSchema = new Schema({
  type: String,
  challengeType: String,
  mode: {
    name: String,
    deck: String,
    cardLevels: String,
    overtimeSeconds: String,
    players: String,
    sameDeck: Boolean
  },
  winCountBefore: Number,
  utcTime: Number,
  deckType: String,
  teamSize: Number,
  winner: Number,
  teamCrowns: Number,
  opponentCrowns: Number,
  team: [teamSchema],
  opponent: [teamSchema],
  arena: {
    name: String,
    arena: String,
    arenaID: Number,
    trophyLimit: Number
  },
  member: {
    type: String,
    ref: 'Member'
  },
  id: String
}, {
  id: true,
  capped: {
    size: 100000000
  }
});

var Battle = db.db.model('Battle', battleSchema);
module.exports = Battle;
