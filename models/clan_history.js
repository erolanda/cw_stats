var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var db = require("../db");

var historySchema = new Schema({
  id: Date,
  donations: Number,
  memberCount: Number,
  score: Number,
  members: [{
    _id: String,
    name: String,
    tag: String,
    rank: Number,
    previousRank: Number,
    role: String,
    expLevel: Number,
    trophies: Number,
    donations: Number,
    donationsReceived: Number,
    donationsDelta: Number,
    arena: {
      name: String
    },
    donationsPercent: Number
  }]
}, {
  id: true,
  capped: {
    size: 100000000
  }
});

var ClanHistory = db.db.model('ClanHistory', historySchema);
module.exports = ClanHistory;
