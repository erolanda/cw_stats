var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var db = require("../db");

var warLogSchema = new Schema({
  createdDate: Number,
  participants: [{
    tag: String,
    name: String,
    cardsEarned: Number,
    battlesPlayed: Number,
    wins: Number
  }],
  standings: [{
    tag: String,
    name: String,
    participants: Number,
    battlesPlayed: Number,
    wins: Number,
    crowns: Number,
    warTrophies: Number,
    warTrophiesChange: Number,
    bagde: {
      name: String,
      category: String,
      id: Number,
      image: String
    }
  }],
  seasonNumber: Number
}, {
  id: true,
  capped: {
    size: 100000000
  }
});

var WarLog = db.db.model('WarLog', warLogSchema);
module.exports = WarLog;
