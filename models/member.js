var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var db = require('../db');

var memberSchema = new Schema({
  name: String,
  tag: String,
  rank: Number,
  previousRank: Number,
  role: String,
  expLevel: Number,
  trophies: Number,
  donations: Number,
  donationsReceived: Number,
  donationsDelta: Number,
  arena: {
    name: String,
    arena: String,
    arenaID: Number,
    trophyLimit: Number
  },
  donationsPercent: Number,
  _id: String
});

var Member = db.db.model('Member', memberSchema);
module.exports = Member;
